/*
 * Getting_Started_with_AVR_Complete_PWM_Driver.c
 *
 * Created: 3/31/2015 0:34:16
 *  Author: Brandy
 */ 

#define F_CPU 1000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

enum {UP,DOWN};

void PWM_Increase_Duty(void);
void PWM_Decrease_Duty(void);
void PWM_BrightDim(void);
void Timer_Frequency(int freq);
void PWM_Init(void);
void milliS_Timer(int milliS);

ISR(TIMER1_COMPA_vect)
{
	PORTB|=(1<<PORTB0);
}
ISR(TIMER1_COMPB_vect)
{
	PORTB&=~(1<<PORTB0);
}
ISR(TIMER0_COMPA_vect)
{
	PWM_BrightDim();
}

void PWM_Increase_Duty(void)
{
	int period = OCR1A;
	int duty = OCR1B;
	if (duty<period)
	{
		duty++;
	} 
	else
	{
		duty = 0;
	}
	OCR1B = duty;	
}

void PWM_Decrease_Duty(void)
{
//	int period = OCR1A;
	int duty = OCR1B;
	if (duty>0)
	{
		duty--;
	} 
	else
	{
		duty = 0;
	}
	OCR1B = duty;	
}

void PWM_BrightDim(void)
{
	int Duty = OCR1B;
	int Period = OCR1A;
	static int direction;
	switch(direction)
	{
		case UP:
			if(++Duty == (Period-1))	
				direction = DOWN;
			break;
		case DOWN:
			if(--Duty == 2)	
				direction = UP;
			break;
	}
	OCR1B = Duty;
}

void Timer_Frequency(int freq)
{
	TCCR1 |=(1<<CTC1)|(0 << CS13)|(0 << CS12)|(1 << CS11)|(1 << CS10);
	/*
		TCCR1 � Timer/Counter1 Control Register
		CTC1 PWM1A COM1A1 COM1A0 CS13 CS12 CS11 CS10
		Clear Timer/Counter on Compare Match
			When the CTC1 control bit is set (one), Timer/Counter1 is reset to $00 
			in the CPU clock cycle after a compare match with OCR1C register value. 
			If the control bit is cleared, Timer/Counter1 continues counting and is unaffected by a compare match.
		Pulse Width Modulator A Enable
		Comparator A Output Mode, Bits 1 and 0
		Clock Select Bits 3, 2, 1, and 0
			The Clock Select bits 3, 2, 1, and 0 define the prescaling source of Timer/Counter1.
			CS11 and CS10 as 1 is the prescaler 102 so the Timer Will Overflow Every 1.907seconds
		pg89
	*/
	TIMSK |=(1 <<OCIE1A);
	/*
		Timer/Counter Interrupt Mask Register
		� OCIE1A OCIE1B OCIE0A OCIE0B TOIE1 TOIE0 � 
		Reserved Bit
		Timer/Counter1 Output Compare Interrupt Enable
			When the OCIE1A bit is set (one) and the I-bit in the Status Register is set (one), 
			the Timer/Counter1 Compare MatchA, interrupt is enabled. 
			The corresponding interrupt at vector $003 is executed if a compare matchA occurs.
			The Compare Flag in Timer/Counter1 is set (one) in the Timer/Counter Interrupt Flag Register.
		Timer/Counter1 Output Compare Interrupt Enable
		Timer/Counter1 Overflow Interrupt Enable
		Reserved Bit
		pg92
	*/
	OCR1A = F_CPU/(freq*2*256)-1;
	/*
		The Timer/Counter Output Compare Register A contains data to be continuously compared with Timer/Counter1.
		Actions on compare matches are specified in TCCR1. A compare match does only occur if Timer/Counter1 counts
		to the OCR1A value. 
	*/
}

void PWM_Init(void)
{
	TCCR1 |=(1<<CTC1)|(0 << CS13)|(0 << CS12)|(0 << CS11)|(1 << CS10);
	/*
		TCCR1 � Timer/Counter1 Control Register
		CTC1 PWM1A COM1A1 COM1A0 CS13 CS12 CS11 CS10
		Clear Timer/Counter on Compare Match
			When the CTC1 control bit is set (one), Timer/Counter1 is reset to $00 
			in the CPU clock cycle after a compare match with OCR1C register value. 
			If the control bit is cleared, Timer/Counter1 continues counting and is unaffected by a compare match.
		Pulse Width Modulator A Enable
		Comparator A Output Mode, Bits 1 and 0
		Clock Select Bits 3, 2, 1, and 0
			The Clock Select bits 3, 2, 1, and 0 define the prescaling source of Timer/Counter1.
			When CS10 is 1, there'll be no prescaler.
		pg.89
	*/
	TIMSK |=(1 <<OCIE1A)|(1<<OCIE1B);
	/*
		Timer/Counter Interrupt Mask Register
		� OCIE1A OCIE1B OCIE0A OCIE0B TOIE1 TOIE0 � 
		Reserved Bit
		Timer/Counter1 Output Compare Interrupt Enable
			When the OCIE1A bit is set (one) and the I-bit in the Status Register is set (one), 
			the Timer/Counter1 Compare MatchA, interrupt is enabled. 
			The corresponding interrupt at vector $003 is executed if a compare matchA occurs.
			The Compare Flag in Timer/Counter1 is set (one) in the Timer/Counter Interrupt Flag Register.
		Timer/Counter1 Output Compare Interrupt Enable
		Timer/Counter1 Overflow Interrupt Enable
		Reserved Bit
		pg92
	*/
	OCR1A = 50;// F_CPU/(10kHz*2*1)-1;
	OCR1B = 10;//Adjustable
	/*
		The Timer/Counter Output Compare Register A contains data to be continuously compared with Timer/Counter1.
		Actions on compare matches are specified in TCCR1. A compare match does only occur if Timer/Counter1 counts
		to the OCR1A value. 
	*/
}

void milliS_Timer(int milliS)
{
	TCCR0A |=(1<<WGM01);
	/*
		COM0A1 COM0A0 COM0B1 COM0B0 � � WGM01 WGM00
		Compare Match Output A Mode
		Compare Match Output B Mode
		Reserved Bits
		Waveform Generation Mode
			0 0 0 Normal             0xFF Immediate MAX(1)
			0 0 1 PWM, Phase Correct 0xFF TOP BOTTOM(2)
			0 1 0 CTC                OCRA Immediate MAX(1) (Selected)
			0 1 1 Fast PWM           0xFF BOTTOM(2) MAX(1)
		pg.79
	*/
	TCCR0B |=(1<CS02)|(0<<CS01)|(1<<CS00);
	/*
		FOC0A FOC0B � � WGM02 CS02 CS01 CS00
		Force Output Compare A
		Force Output Compare B
		Reserved Bits
		Reserved Bits
		Waveform Generation Mode
		Clock Select
			CS02 CS01 CS00 Description
			 0    0    0   No clock source (Timer/Counter stopped)
			 0    0    1   clk(I/O)/(No prescaling)
			 0    1    0   clk(I/O)/8 (From prescaler)
			 0    1    1   clk(I/O)/64 (From prescaler)
			 1    0    0   clk(I/O)/256 (From prescaler)
			 1    0    1   clk(I/O)/1024 (From prescaler) (Selected)
			 1    1    0   External clock source on T0 pin. Clock on falling edge.
			 1    1    1   External clock source on T0 pin. Clock on rising edge.
		pg.80
	*/
	TIMSK |= (1<<OCIE0A);
	/*
		� OCIE1A OCIE1B OCIE0A OCIE0B TOIE1 TOIE0 �
		Reserved Bits
		Timer/Counter0 Output Compare Match A Interrupt Enable
		Timer/Counter Output Compare Match B Interrupt Enable
		Timer/Counter0 Overflow Interrupt Enable
		Reserved Bits
	*/
	OCR0A = milliS*7.8125-1;
}

int main(void)
{
	DDRB |= (1 << DDB0);
	DDRB &= ~(1 << DDB1);
	sei();
	milliS_Timer(2);
	PWM_Init();
    while(1)
    {
        //TODO:: Please write your application code 
    }
}